import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { startTimer, resetTimer, stopTimer } from "../../modules/timer";

const Timer = ({ startTimer, stopTimer, resetTimer, timer: { elapsed } }) => {
  let ms = elapsed / 10;
  let seconds = Math.floor(elapsed / 1000);
  let minutes = Math.floor(seconds / 60);
  let hours = Math.floor(minutes / 60);
  ms = (ms % 100).toString().padStart(2, "0");
  seconds = (seconds % 60).toString().padStart(2, "0");
  minutes = (minutes % 60).toString().padStart(2, "0");
  hours = (hours % 24).toString().padStart(2, "0");
  return (
    <div>
      <h2>
        {hours}h:
        {minutes}m:
        {seconds}s:
        {ms}
      </h2>
      <div>
        <button onClick={startTimer}>start</button>
        <button onClick={stopTimer}>pause</button>
        <button onClick={resetTimer}>reset</button>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  timer: state.timer
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ startTimer, resetTimer, stopTimer }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Timer);
