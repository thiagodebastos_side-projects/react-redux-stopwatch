// @flow
// https://flow.org/en/docs/react/redux/

// types

type StartTimerAction = {| type: "timer/TIMER_START" |};
type StopTimerAction = {| type: "timer/TIMER_STOP" |};
type TickTimerAction = {| type: "timer/TIMER_TICK" |};
type ResetTimerAction = {| type: "timer/RESET_TIMER" |};

type Action = StartTimerAction | PauseTimerAction | ResetTimerAction;

type State = {
  +elapsed: number,
  +isRunning: boolean
};

// action types

export const TIMER_START = "timer/TIMER_START";
export const TIMER_STOP = "timer/TIMER_STOP";
export const TIMER_TICK = "timer/TIMER_TICK";
export const RESET_TIMER = "timer/RESET_TIMER";

// state initialisation

const initialState = {
  elapsed: 0,
  isRunning: false
};

// reducer
export default (state: State = initialState, action: Action) => {
  switch (action.type) {
    case TIMER_START:
      if (state.isRunning) return state;
      return { ...state, isRunning: true };

    case TIMER_STOP:
      if (!state.isRunning) return state;
      return { ...state, isRunning: false };

    case RESET_TIMER:
      stopTimer();
      return initialState;

    case TIMER_TICK:
      return { ...state, elapsed: state.elapsed + 10 };

    default:
      // validate that you have handled every single type of action
      (action: empty); // eslint-disable-line
      return state;
  }
};

// action creators

/* NOTE:
  This seems like overkill, we are dispatching an action every 10ms
  a better method has been discussed here: https://stackoverflow.com/questions/34577012/creating-a-stopwatch-with-redux
  let redux worry about setting a timestamp for when the timer started/stopped
  let the component decide when to update/render changes based on these timestamps
*/

let timer = null;

export function startTimer(): StartTimerAction {
  return (dispatch, getState) => {
    if (getState().timer.isRunning === false) {
      clearInterval(timer);
      timer = setInterval(() => dispatch(tickTimer()), 10);
      dispatch({ type: TIMER_START });
      dispatch(tickTimer());
    }
  };
}

function tickTimer(): TickTimerAction {
  return { type: TIMER_TICK };
}

export function stopTimer(): StopTimerAction {
  clearInterval(timer);
  return { type: TIMER_STOP };
}

export function resetTimer(): ResetTimerAction {
  return dispatch => dispatch({ type: RESET_TIMER });
}
