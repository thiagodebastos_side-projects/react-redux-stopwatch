import React from "react";
import { hot } from "react-hot-loader";
import { Route, Link } from "react-router-dom";
import TimerApp from "../timer";
import About from "../about";

const App = () => (
  <div>
    <header>
      <Link to="/">TimerApp</Link>
      <Link to="/about-us">About</Link>
    </header>

    <main>
      <Route exact path="/" component={TimerApp} />
      <Route exact path="/about-us" component={About} />
    </main>
  </div>
);

export default hot(module)(App);
